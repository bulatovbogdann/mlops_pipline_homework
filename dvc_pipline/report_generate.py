from dvc.api import DVCFileSystem

url = 'https://gitlab.com/bulatovbogdann/mlops_pipline_homework.git'

fs_dev  = DVCFileSystem(url, rev="dev")
file_dev = fs_dev.read_text('/dvc_pipline/summary.json', encoding="utf-8")

fs_main  = DVCFileSystem(url, rev="main")
file_main = fs_main.read_text('/dvc_pipline/summary.json', encoding="utf-8")