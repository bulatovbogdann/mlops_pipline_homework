# Описание
Пример построения и автоматизации экспериментов ML с помощью DVC и инструментов gitlab-ci

В качестве исходных данных использовался датасет c отзывами [Amazon](https://www.kaggle.com/datasets/kritanjalijain/amazon-reviews)

В качестве базовых моделей были использованы:
- Logistic Regression 
- Random Forest

В рамках данного проекта акцент сделан на построении процессов по получению и обработке данных, с последующим обучением модели, сохранением метрик и автоматизацией итогового отчёта 

Ссылка на пайлайн в ci (Log_reg):
https://gitlab.com/bulatovbogdann/mlops_pipline_homework/-/jobs/6794282836

Ссылка на пайлайн в ci (Random_forest):
https://gitlab.com/bulatovbogdann/mlops_pipline_homework/-/jobs/6793964682

Ссылка на пайлайн в ci (Создание отчёта):
https://gitlab.com/bulatovbogdann/mlops_pipline_homework/-/jobs/6796486252

Удаленное хранилище: 
https://drive.google.com/drive/u/0/folders/1OGaddm23KriOwcfIT0zJAnxsAdIj4rP5

Итоговый отчёт в gitlab pages: 
https://bulatovbogdann.gitlab.io/mlops_pipline_homework/